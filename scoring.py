# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas.tools.plotting import scatter_matrix
from sklearn.grid_search import GridSearchCV
from sklearn.cross_validation import train_test_split
from sklearn.neighbors import KNeighborsClassifier

def PlotXYFeatures(col1, col2):
    plt.scatter(data[col1][data['class'] == '+'],
                data[col2][data['class'] == '+'],
                color = ['red'],
                alpha = 0.5,
                label = ['+'])
    plt.scatter(data[col1][data['class'] == '-'],
                data[col2][data['class'] == '-'],
                color = ['blue'],
                alpha = 0.5,
                label = ['-'])
    plt.xlabel(col1)
    plt.ylabel(col2)
    plt.show()

# plt.style.use('ggplot')
data = pd.read_csv('crx.data.txt', header=None, na_values='?')

# Печать размеров таблицы
print data.shape
print '\n'

# Печатаем первые 3 элементов таблицы
print data.head(3)
print '\n'

# Печатаем последние 3 элементов таблицы
print data.tail(3)
print '\n'

# Переиминовываем названия всех столбцов
data.columns = ['A' + str(i) for i in range(0, 15)] + ['class']

# Вывод значений по конкретным элементам
print data['A5'][10:15]
print '\n'
print data.at[10, 'A5']
print '\n'

# Как вывести значения по строке?

# Вывести статистику по таблице
print data.describe()
print '\n'

# Вывод всех названий столбцов и типа столбца
print data.columns
print data['A5'].dtype
print '\n'

# Выделяем все категориальные и количественные признаки
categorial = [c for c in data.columns if data[c].dtype == 'object']
numerical = [c for c in data.columns if data[c].dtype != 'object']
print categorial
print numerical
print '\n'

# Получим информацию по всем количественным признакам
categorial = [c for c in data.columns if data[c].dtype == 'object']
numerical = [c for c in data.columns if data[c].dtype != 'object']
print categorial
print numerical
print '\n'

# Печатаем информацию (базовая) о количественных данных
print data[categorial].describe()

# Печатаем информацию (базовая) о номинальных данных
print data[categorial].describe(include=['object'])

# Выводим список уникальных (категориальных) объектов
for value in categorial:
    print data[value].unique()

# Выводим диаграмму рассеяния. alpha -- прозрачность. diagonal='kde' -- непрерывные графики на диагонали (если не указан,
# то гистограммы)
# scatter_matrix(data, alpha=0.3, figsize=(10, 10), diagonal='kde');
# plt.show()

# Выведем корелляцию для количественных признаков
print data.corr()

# Выводим данные по признакам
# PlotXYFeatures('A1', 'A10')


#########################
# Подготовка данных

# Считаем число значений в столбцах
print data.count(axis=0)
print data.count(axis=1)
print data.count(axis=0)

# Удаляем столбцы (axis = 1) или строки (axis = 0) с пустыми значениями и дубликаты
# data = data.drop_duplicates()
# data = data.dropna(axis = 0)
# print data.describe()

# Заполняем пропущенные значения медианными значениями
data = data.fillna(data.median(axis = 0), axis = 0)
print data.describe(include=['object'])

# Заполняем пропущенные значения в номинальных признаках самыми часто встречающимися значенями
data_describe = data.describe(include = ['object'])
for column in categorial:
    data[column] = data[column].fillna(data_describe[column]['top'])

# Выделяем бинарные признаки и небинарные признаки
binary_features = [feature for feature in categorial if data_describe[feature]['unique'] == 2]
nonbinary_features = [feature for feature in categorial if data_describe[feature]['unique'] > 2]

# Заменяем все значения на количественные
for c in binary_features:
    top = data_describe[c]['top']
    top_items = data[c] == top
    data.loc[top_items, c] = 0
    data.loc[np.logical_not(top_items), c] = 1

# Смотрим результат
print data[binary_features].describe()

# Строим из небинарных признаков бинарные
data_nonbinary = pd.get_dummies(data[nonbinary_features])
print data_nonbinary.describe()

# Нормализуем количественные признаки
data_numerical = data[numerical]
# data_numerical = (data_numerical - data_numerical.mean()) / data_numerical.std() # Нулевая дисперсия
print data_numerical.describe()

# Объединяем все данные в единую таблицу
data = pd.concat((data_numerical, data[binary_features], data_nonbinary), axis=1)
data = pd.DataFrame(data, dtype=float)
print data.shape
print data.columns

# Выделяем классы в отдельный вектор
input = data.drop(('class'), axis=1)  # Выбрасываем столбец 'class'.
target = data['class']
feature_names = input.columns
n, f = input.shape
print n, f

# Подготовка обучающей и тестовой выборок
# random_state -- случаное число для построения разбиения
X_train, X_test, y_train, y_test = train_test_split(input, target, test_size = 0.3, random_state = 11)
N_train, _ = X_train.shape
N_test,  _ = X_test.shape
print N_train, N_test

#########################
# Метод kNN

# Инициализация и обучение
knn = KNeighborsClassifier(n_neighbors=4, leaf_size=15, p=10)
knn.fit(X_train, y_train)

# Качество
y_train_predict = knn.predict(X_train)
y_test_predict = knn.predict(X_test)

# Среднее число ошибок
err_train = np.mean(y_train != y_train_predict)
err_test  = np.mean(y_test  != y_test_predict)
print err_train, err_test

# Cross-validation
n_neighbors_array = range(7, 10)
p_array = range(10, 14)
leaf_size_array = range(10, 12)
grid = GridSearchCV(knn, param_grid={'n_neighbors': n_neighbors_array, 'p': p_array, 'leaf_size': leaf_size_array})
grid.fit(X_train, y_train)

best_cv_err = 1 - grid.best_score_
best_n_neighbors = grid.best_estimator_.n_neighbors
best_p = grid.best_estimator_.p
best_leaf_size = grid.best_estimator_.leaf_size
print best_cv_err, best_n_neighbors, best_p, best_leaf_size

# После cross-validation будем использовать подобранные параметры
knn = KNeighborsClassifier(n_neighbors=best_n_neighbors, p=best_p, leaf_size=best_leaf_size)
knn.fit(X_train, y_train)

err_train = np.mean(y_train != knn.predict(X_train))
err_test  = np.mean(y_test  != knn.predict(X_test))
print err_train, err_test


#########################
# SVM
from sklearn.svm import SVC
svc = SVC()
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print err_train, err_test

# Радиальное ядро (cross-validation)
from sklearn.grid_search import GridSearchCV
C_array = np.logspace(-3, 3, num=7)
gamma_array = np.logspace(-5, 2, num=8)
svc = SVC(kernel='rbf')
grid = GridSearchCV(svc, param_grid={'C': C_array, 'gamma': gamma_array})
grid.fit(X_train, y_train)
print 'CV error    = ', 1 - grid.best_score_
print 'best C      = ', grid.best_estimator_.C
print 'best gamma  = ', grid.best_estimator_.gamma

# Тестирование после cross-validation
svc = SVC(kernel='rbf', C=grid.best_estimator_.C, gamma=grid.best_estimator_.gamma)
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print err_train, err_test


#########################
# RF
from sklearn import ensemble
rf = ensemble.RandomForestClassifier(n_estimators=100, random_state=11)
rf.fit(X_train, y_train)

err_train = np.mean(y_train != rf.predict(X_train))
err_test  = np.mean(y_test  != rf.predict(X_test))
print err_train, err_test